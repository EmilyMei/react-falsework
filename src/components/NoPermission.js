import React from 'react';
import {Button} from 'antd';
import './NotFound.less';
const NoPermission = () => (
  <div className='normal'>
    <div className='container'>
      <div className='img-box'><div className='img'></div></div>
      <div className='txt-box'>
        <h3 className='tit'>抱歉，您无权限～～</h3>
        <p className='description'>抱歉，您暂无权限，请看看其他页面</p>
        <a href='/'>
          <Button type='primary' style={{
            marginTop: 20
          }}>返回首页</Button>
        </a>
        <a href='/#/login'>
          <Button type='primary' style={{
            marginTop: 20,
            marginLeft: 40
          }}>登录</Button>
        </a>
      </div>
    </div>
  </div>
);

export default NoPermission;
