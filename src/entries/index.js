import ReactDOM from 'react-dom';
import React from 'react';
import {
  hashHistory,
  Router,
  Route,
  IndexRedirect
} from 'react-router';
import App from '../views/App';
import { message } from 'antd';

import Home from '../views/Home';
import TableMain from '../views/Table';
import ChartMain from '../views/ChartMain';
import FormMain from '../views/FormMain';
const Provider = require('react-redux').Provider;
import configureStore  from '../store/configureStore';
import { getCookie } from '../util';
import NoPermission from '../components/NoPermission';

const store = configureStore();
// 判断是否有访问路由的权限
function onEnterRouter(nt, rp, cb, powerCode){
  const codeArray = JSON.parse(localStorage.getItem('userPermissions'));
  // 判断用户是否登录
  if (!codeArray || (!getCookie('token') && nt.location.pathname !== '/login') ) {
    message.error('请先登录');
    hashHistory.push('/login');
  } else {
    // 判断用户是否有访问该路由的权限
    if (codeArray.findIndex((val) => val === powerCode) !== -1) {
      cb();
    } else {
      location.href='/#/nopermission';
    }
  } 
}

function validate(next, replace, callback) {
  //验证
  if(!!getCookie('token') === false && next.location.pathname !== '/login'){
    hashHistory.push('/login');
  }
  callback();
}
ReactDOM.render(
  <Provider store={store}>
    <Router history={hashHistory} onEnter={validate}>
      <Route path='/' onEnter={validate}>
        {/* 正常情况默认首页 */}
        <IndexRedirect to='index' />
        <Route component={App}>
          {/* 首页 */}
          <Route path='index' component={Home} />
          <Route path='table' component={TableMain} />
          <Route path='chart' component={ChartMain}  />
          <Route path='form' component={FormMain}  />
        </Route>
      </Route>
      {/* 登录 */}
      {/* <Route path='login' component={Login} /> */}
      <Route path='nopermission' component={NoPermission} />
      {/* <Route path='*' component={NotFound} /> */}
    </Router>
  </Provider>, document.getElementById('root'));
